# gameGame

## Game Idea

A roguelike that is focused on summons which fight for you.

# Basic Idea of the Game:
You are a summoner and are tasked to go room through room fighting enemies.

Every now and then you can get a new summon or an upgrade for your existing ones.

The summoner is the only one that can get killed.

The summoner can upgrade himself or his summons.

the gameplay should be similar the binding of isaac.