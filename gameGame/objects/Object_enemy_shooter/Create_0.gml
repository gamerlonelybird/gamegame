/// @DnDAction : YoYo Games.Movement.move_and_collide
/// @DnDVersion : 1
/// @DnDHash : 7990B6A5
/// @DnDArgument : "object" "noone"
move_and_collide(0, 0, noone,4,0,0,0,0);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 1A8ABA33
/// @DnDArgument : "expr" "3"
/// @DnDArgument : "var" "EnemySpeed"
EnemySpeed = 3;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 79CE58E9
/// @DnDArgument : "expr" "3"
/// @DnDArgument : "var" "EnemyHealth"
EnemyHealth = 3;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 3251D2AF
/// @DnDArgument : "steps" "60"
alarm_set(0, 60);