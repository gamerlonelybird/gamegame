/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 279C9E30
/// @DnDArgument : "var" "point_in_circle(obj_player.x, obj_player.y, x, y, 200)"
if(point_in_circle(obj_player.x, obj_player.y, x, y, 200) == 0)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
	/// @DnDVersion : 1
	/// @DnDHash : 18A7CB9B
	/// @DnDParent : 279C9E30
	/// @DnDArgument : "x" "obj_player.x"
	/// @DnDArgument : "y" "obj_player.y"
	direction = point_direction(x, y, obj_player.x, obj_player.y);

	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 0A905BC4
	/// @DnDParent : 279C9E30
	/// @DnDArgument : "speed" "EnemySpeed"
	speed = EnemySpeed;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 0BF2F074
else
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3C00A570
	/// @DnDParent : 0BF2F074
	/// @DnDArgument : "var" "point_in_circle(obj_player.x, obj_player.y, x, y, 100)"
	if(point_in_circle(obj_player.x, obj_player.y, x, y, 100) == 0)
	{
		/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
		/// @DnDVersion : 1
		/// @DnDHash : 5A4F5D11
		/// @DnDParent : 3C00A570
		/// @DnDArgument : "x" "obj_player.x"
		/// @DnDArgument : "y" "obj_player.y"
		direction = point_direction(x, y, obj_player.x, obj_player.y);
	
		/// @DnDAction : YoYo Games.Movement.Set_Speed
		/// @DnDVersion : 1
		/// @DnDHash : 6CC88BA1
		/// @DnDParent : 3C00A570
		/// @DnDArgument : "speed" "-EnemySpeed"
		speed = -EnemySpeed;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 0AE912AA
	/// @DnDParent : 0BF2F074
	else
	{
		/// @DnDAction : YoYo Games.Movement.Set_Speed
		/// @DnDVersion : 1
		/// @DnDHash : 2084C38A
		/// @DnDParent : 0AE912AA
		speed = 0;
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7A487997
/// @DnDArgument : "var" "EnemyHealth"
if(EnemyHealth == 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 012A0644
	/// @DnDParent : 7A487997
	instance_destroy();
}