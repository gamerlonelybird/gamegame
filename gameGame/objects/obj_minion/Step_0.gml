/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 47E22C38
/// @DnDArgument : "code" "/// calculate distance from player$(13_10)distancey = y - obj_player.y;$(13_10)distancex = x - obj_player.x;$(13_10)distance = sqrt(distancex * distancex + distancey * distancey);"
/// calculate distance from player
distancey = y - obj_player.y;
distancex = x - obj_player.x;
distance = sqrt(distancex * distancex + distancey * distancey);

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 7AF28446
/// @DnDArgument : "x" "x + hspeed"
/// @DnDArgument : "y" "y + vspeed"
/// @DnDArgument : "object" "Object_Block"
/// @DnDSaveInfo : "object" "Object_Block"
var l7AF28446_0 = instance_place(x + hspeed, y + vspeed, [Object_Block]);
if ((l7AF28446_0 > 0))
{
	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 5FA8AB14
	/// @DnDParent : 7AF28446
	/// @DnDArgument : "speed" "-2"
	speed = -2;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 55C96392
else
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6216BAAD
	/// @DnDParent : 55C96392
	/// @DnDArgument : "var" "distance"
	/// @DnDArgument : "op" "2"
	/// @DnDArgument : "value" "200"
	if(distance > 200)
	{
		/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
		/// @DnDVersion : 1
		/// @DnDHash : 0292353A
		/// @DnDParent : 6216BAAD
		/// @DnDArgument : "x" "obj_player.x"
		/// @DnDArgument : "y" "obj_player.y"
		direction = point_direction(x, y, obj_player.x, obj_player.y);
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6DBDFF41
		/// @DnDParent : 6216BAAD
		/// @DnDArgument : "var" "speed"
		/// @DnDArgument : "not" "1"
		/// @DnDArgument : "value" "8"
		if(!(speed == 8))
		{
			/// @DnDAction : YoYo Games.Movement.Set_Speed
			/// @DnDVersion : 1
			/// @DnDHash : 74DBCD89
			/// @DnDParent : 6DBDFF41
			/// @DnDArgument : "speed" "2"
			/// @DnDArgument : "speed_relative" "1"
			speed += 2;
		}
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 5F0B9188
	/// @DnDParent : 55C96392
	else
	{
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 05EFA538
		/// @DnDParent : 5F0B9188
		/// @DnDArgument : "var" "speed"
		/// @DnDArgument : "not" "1"
		/// @DnDArgument : "op" "3"
		if(!(speed <= 0))
		{
			/// @DnDAction : YoYo Games.Movement.Set_Speed
			/// @DnDVersion : 1
			/// @DnDHash : 52F3A6F8
			/// @DnDParent : 05EFA538
			/// @DnDArgument : "speed" "-1"
			/// @DnDArgument : "speed_relative" "1"
			speed += -1;
		}
	}
}