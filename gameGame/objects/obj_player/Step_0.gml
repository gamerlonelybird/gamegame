/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 09415EBC
/// @DnDArgument : "code" "/// Player Speed$(13_10)PlayerSpeedDiagonal = sqrt(sqr(PlayerSpeed) / 2)$(13_10)$(13_10)/// Movement code$(13_10) _right = keyboard_check(vk_right) or keyboard_check(ord("D"));$(13_10) _left = keyboard_check(vk_left) or keyboard_check(ord("A"));$(13_10) _down = keyboard_check(vk_down) or keyboard_check(ord("S"));$(13_10) _up = keyboard_check(vk_up) or keyboard_check(ord("W"));$(13_10)$(13_10) _xinput = _right - _left;$(13_10) _yinput = _down - _up;$(13_10)$(13_10)if( _xinput == 1 && _yinput == 1 ||  _xinput == -1 && _yinput == -1 ||  _xinput == 1 && _yinput == -1 ||  _xinput == -1 && _yinput == 1)$(13_10){$(13_10)	move_and_collide(_xinput * PlayerSpeedDiagonal, _yinput * PlayerSpeedDiagonal, Object_Block);$(13_10)	direction = point_direction(x, y, x + _xinput * PlayerSpeedDiagonal, y + _yinput * PlayerSpeedDiagonal);$(13_10)	_moved = 1;$(13_10)	historical_yinput = _yinput$(13_10)	historical_xinput = _xinput$(13_10)}$(13_10)else if (_xinput != 0 || _yinput != 0)$(13_10){$(13_10)	move_and_collide(_xinput * PlayerSpeed, _yinput * PlayerSpeed, Object_Block);$(13_10)	direction = point_direction(x, y, x + _xinput * PlayerSpeed, y + _yinput * PlayerSpeed);$(13_10)	_moved = 1;$(13_10)	historical_yinput = _yinput$(13_10)	historical_xinput = _xinput$(13_10)}$(13_10)$(13_10)if (_right == 0 && _left == 0 && _up == 0 && _down == 0)$(13_10){$(13_10)	if (_moved >= 1)$(13_10)	{$(13_10)		if(_moved < 6)$(13_10)		{$(13_10)			if( historical_xinput == 1 && historical_yinput == 1 ||  historical_xinput == -1 && historical_yinput == -1 ||  historical_xinput == 1 && historical_yinput == -1 ||  historical_xinput == -1 && historical_yinput == 1)$(13_10)			{$(13_10)				move_and_collide(historical_xinput * PlayerSpeedDiagonal /_moved, historical_yinput * PlayerSpeedDiagonal /_moved, Object_Block);$(13_10)			}$(13_10)			else if (historical_xinput != 0 || historical_yinput != 0)$(13_10)			{$(13_10)				move_and_collide(historical_xinput * PlayerSpeed /_moved, historical_yinput * PlayerSpeed /_moved, Object_Block);$(13_10)			}$(13_10)			_moved += 1$(13_10)		}$(13_10)		 if (_moved == 6)$(13_10)		{$(13_10)			_moved = 0;$(13_10)		}$(13_10)$(13_10)	}$(13_10)}"
/// Player Speed
PlayerSpeedDiagonal = sqrt(sqr(PlayerSpeed) / 2)

/// Movement code
 _right = keyboard_check(vk_right) or keyboard_check(ord("D"));
 _left = keyboard_check(vk_left) or keyboard_check(ord("A"));
 _down = keyboard_check(vk_down) or keyboard_check(ord("S"));
 _up = keyboard_check(vk_up) or keyboard_check(ord("W"));

 _xinput = _right - _left;
 _yinput = _down - _up;

if( _xinput == 1 && _yinput == 1 ||  _xinput == -1 && _yinput == -1 ||  _xinput == 1 && _yinput == -1 ||  _xinput == -1 && _yinput == 1)
{
	move_and_collide(_xinput * PlayerSpeedDiagonal, _yinput * PlayerSpeedDiagonal, Object_Block);
	direction = point_direction(x, y, x + _xinput * PlayerSpeedDiagonal, y + _yinput * PlayerSpeedDiagonal);
	_moved = 1;
	historical_yinput = _yinput
	historical_xinput = _xinput
}
else if (_xinput != 0 || _yinput != 0)
{
	move_and_collide(_xinput * PlayerSpeed, _yinput * PlayerSpeed, Object_Block);
	direction = point_direction(x, y, x + _xinput * PlayerSpeed, y + _yinput * PlayerSpeed);
	_moved = 1;
	historical_yinput = _yinput
	historical_xinput = _xinput
}

if (_right == 0 && _left == 0 && _up == 0 && _down == 0)
{
	if (_moved >= 1)
	{
		if(_moved < 6)
		{
			if( historical_xinput == 1 && historical_yinput == 1 ||  historical_xinput == -1 && historical_yinput == -1 ||  historical_xinput == 1 && historical_yinput == -1 ||  historical_xinput == -1 && historical_yinput == 1)
			{
				move_and_collide(historical_xinput * PlayerSpeedDiagonal /_moved, historical_yinput * PlayerSpeedDiagonal /_moved, Object_Block);
			}
			else if (historical_xinput != 0 || historical_yinput != 0)
			{
				move_and_collide(historical_xinput * PlayerSpeed /_moved, historical_yinput * PlayerSpeed /_moved, Object_Block);
			}
			_moved += 1
		}
		 if (_moved == 6)
		{
			_moved = 0;
		}

	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7A9E8EA4
/// @DnDArgument : "var" "__dnd_lives"
/// @DnDArgument : "op" "3"
if(__dnd_lives <= 0)
{
	/// @DnDAction : YoYo Games.Game.Restart_Game
	/// @DnDVersion : 1
	/// @DnDHash : 590C17C2
	/// @DnDParent : 7A9E8EA4
	game_restart();
}