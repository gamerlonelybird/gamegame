/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
/// @DnDVersion : 1
/// @DnDHash : 625603E1
/// @DnDArgument : "sprite" "spr_health"
/// @DnDSaveInfo : "sprite" "spr_health"
var l625603E1_0 = sprite_get_width(spr_health);
var l625603E1_1 = 0;
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
for(var l625603E1_2 = __dnd_lives; l625603E1_2 > 0; --l625603E1_2) {
	draw_sprite(spr_health, 0, 0 + l625603E1_1, 0);
	l625603E1_1 += l625603E1_0;
}