/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 2D237704
/// @DnDArgument : "expr" "10"
/// @DnDArgument : "var" "PlayerSpeed"
PlayerSpeed = 10;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 11333427
/// @DnDArgument : "expr" "sqrt(sqr(PlayerSpeed) / 2)"
/// @DnDArgument : "var" "PlayerSpeedDiagonal"
PlayerSpeedDiagonal = sqrt(sqr(PlayerSpeed) / 2);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 04E68D35
/// @DnDArgument : "var" "_moved"
_moved = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 5E6533D6
/// @DnDArgument : "var" "setSpeed"
setSpeed = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 09E76DAA
/// @DnDArgument : "var" "historical_xinput"
historical_xinput = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 320B9788
/// @DnDArgument : "var" "historical_yinput"
historical_yinput = 0;

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 017E4902
/// @DnDArgument : "lives" "5"

__dnd_lives = real(5);